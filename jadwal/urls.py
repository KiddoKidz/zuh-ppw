from django.urls import path, include
from . import views

urlpatterns = [
    path('',views.schedule_list,name='schedule_list'),
    path('add/', views.schedule, name='schedule'),
    path('activity/<int:activity_id>/delete/',views.schedule_delete,name='delete'),
    path('activity/<int:activity_id>/edit/',views.schedule_edit,name='edit'),
]