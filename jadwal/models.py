from django.db import models

# Create your models here.
class Schedule(models.Model):
    activity_detail = models.CharField(max_length=30)
    category = models.CharField(max_length = 30)
    place = models.CharField(max_length=30)
    day = models.CharField(max_length=10)
    date = models.DateField()
    time = models.TimeField()
    description = models.TextField()

    def __str__(self):
        return self.activity_detail