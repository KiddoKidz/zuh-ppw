from django.shortcuts import render, redirect, reverse, get_object_or_404
from .forms import ScheduleForm
from .models import Schedule
from django.http import HttpResponse, HttpResponseRedirect
from datetime import datetime


# Create your views here.

def schedule(request):
    if(request.method == "POST"):
        if(int(request.POST["id"])>=0):
            schedule_item = get_object_or_404(Schedule, id=request.POST["id"])
            dateArray = request.POST["date"].split("|")
            day = dateArray[0].strip()
            date = dateArray[1].strip()
            schedule_item.activity_detail = request.POST["activity_title"]
            schedule_item.category = request.POST["category"]
            schedule_item.place = request.POST["place"]
            schedule_item.day = day
            schedule_item.date = date
            schedule_item.time = request.POST["time"]
            schedule_item.description = request.POST["description"]
            schedule_item.save()
            return HttpResponseRedirect(reverse('jadwal:schedule_list'))
        else:
            data = request.POST
            dateArray = data["date"].split("|")
            day = dateArray[0].strip()
            date = dateArray[1].strip()
            sched_form = ScheduleForm(data)
            if(sched_form.is_valid):
                print(request.POST)
                new_schedule = Schedule()
                new_schedule.activity_detail = data["activity_title"]
                new_schedule.category = data["category"]
                new_schedule.place = data["place"]
                new_schedule.day = day
                new_schedule.date = date
                new_schedule.time = data["time"]
                new_schedule.description = data["description"]
                new_schedule.save()
                return HttpResponseRedirect(reverse('jadwal:schedule_list'))
            else:
                return HttpResponseRedirect(reverse('jadwal:schedule'))
    elif(request.method == "GET"):
        schedule_form = ScheduleForm()
        return render(request, "jadwal.html",{'form_jadwal':schedule_form,'activity_id':-1})



def schedule_list(request):
    if(request.method == "GET"):
        list_day = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
        activities = Schedule.objects.all()
        return render(request, "list_jadwal.html", {'list_day':list_day, 'list_activity':activities})
    else:
        return HttpResponseRedirect(reverse('jadwal:schedule_list'))

def schedule_delete(request, activity_id):
    schedule_item = Schedule.objects.get(id=int(activity_id))
    schedule_item.delete()
    return HttpResponseRedirect(reverse('jadwal:schedule_list'))

def schedule_edit(request, activity_id):

    schedule_item = get_object_or_404(Schedule, id=activity_id)
    day = schedule_item.day
    date = schedule_item.date.strftime("%Y-%m-%d")
    fulldate = day + " | " + date
    time = schedule_item.time.strftime("%H:%M")

    data = {'activity_title': schedule_item.activity_detail, 'category':schedule_item.category, 'place':schedule_item.place, 'date':fulldate, 'time':time, 'description':schedule_item.description}

    schedule_form = ScheduleForm(data)
    return render(request, "jadwal.html", {'form_jadwal':schedule_form,'activity_id':schedule_item.id})
