from django import forms
from django.conf.global_settings import DATE_INPUT_FORMATS

class ScheduleForm(forms.Form):
    activity_title=forms.CharField(max_length=30,widget=forms.TextInput(attrs={'class':'validate','id':'nama_kegiatan', 'tag':'Nama Kegiatan'}))
    category = forms.CharField(max_length=30,widget=forms.TextInput(attrs={'class':'validate','id':'kategori','tag':'Kategori'}))
    place = forms.CharField(max_length=30,widget=forms.TextInput(attrs={'class':'validate','id':'tempat','tag':'Tempat'}))
    date = forms.DateField(widget=forms.DateInput(attrs={'id':'tanggal','class':'datepicker validate', 'tag':'Tanggal Kegiatan'}))
    time = forms.TimeField(widget=forms.TimeInput(format='%H:%M',attrs={'class':'timepicker validate','id':'jam', 'tag':'Jam'}))
    description = forms.CharField(max_length=500,widget=forms.Textarea(attrs={'class':'materialize-textarea','id':'deskripsi','tag':'Deskripsi'}))

    # def __init__(self, activity_title, category, place,date,time,description):
    #     super().__init__()
    #     activity_title=forms.CharField(max_length=30,widget=forms.TextInput(attrs={'class':'validate','id':'nama_kegiatan', 'tag':'Nama Kegiatan', 'value':activity_title}))
    #     category = forms.CharField(max_length=30,widget=forms.TextInput(attrs={'class':'validate','id':'kategori','tag':'Kategori', 'value':category}))
    #     place = forms.CharField(max_length=30,widget=forms.TextInput(attrs={'class':'validate','id':'tempat','tag':'Tempat','value':place}))
    #     date = forms.DateField(widget=forms.DateInput(attrs={'id':'tanggal','class':'datepicker validate', 'tag':'Tanggal Kegiatan', 'value':date}))
    #     time = forms.TimeField(widget=forms.TimeInput(format='%H:%M',attrs={'class':'timepicker validate','id':'jam', 'tag':'Jam', 'value':time}))
    #     description = forms.CharField(max_length=500,widget=forms.Textarea(attrs={'class':'materialize-textarea','id':'deskripsi','tag':'Deskripsi', 'value':description}))
