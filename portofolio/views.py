from django.shortcuts import render

# Create your views here.


def portofolio(request):
    return render(request, 'index.html')

def form(request):
    return render(request, 'form.html')
